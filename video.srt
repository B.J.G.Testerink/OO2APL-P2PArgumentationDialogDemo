1
00:00:00,000 --> 00:00:10,000
At the top part of the visualization we see 
who is talking to who

2
00:00:10,100 --> 00:00:17,000
We click on Alice to show her dialogue graph 

3
00:00:17,100 --> 00:00:21,000
The dialogue graph is split in five views

4
00:00:21,100 --> 00:00:30,000
Here, Alice argues that Carl is a fraud

5
00:00:30,100 --> 00:00:40,000
The intake agent asks two extra questions

6
00:00:40,100 --> 00:00:47,000
We will now look at the locution coherence

7
00:00:47,100 --> 00:00:55,000
The dialogue is interpreted as three sub-dialogues 

8
00:00:55,100 --> 00:01:00,000
One about Carl being a fraud

9
00:01:00,100 --> 00:01:16,000
One about Carl giving a location, 
and one about Carl sending a passport photo

10
00:01:16,100 --> 00:01:25,000
This graph shows the arguments that were made

11
00:01:25,100 --> 00:01:35,000
For instance, it was argued that Carl is a fraud

12
00:01:35,100 --> 00:01:45,000
The intake agent does not commit to this, though

13
00:01:45,100 --> 00:02:00,000
The illocutionary force tab shows how locutions 
contributed to the argument 1 graph.

14
00:02:00,100 --> 00:02:15,000
Here we see Alice's since locution that 
asserted the argument that Carl is a fraud

15
00:02:15,100 --> 00:02:31,000
The concede locution here committed the intake agent

16
00:02:31,100 --> 00:02:40,000
The AIF tab shows the connections to the 
AIF ontology

17
00:02:40,100 --> 00:02:55,000
For instance propositions are information nodes

18
00:03:00,100 --> 00:03:10,000
Notice how the detective only sees 
one response locution

19
00:03:10,100 --> 00:03:25,000
The analyst discussed this question with 
the intake and requisition agents

20
00:03:25,100 --> 00:03:37,000
Together they conclude that Carl is 
not a fraud