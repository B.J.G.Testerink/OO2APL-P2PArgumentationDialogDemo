This archive contains the demo "Developing Argumentation Dialogues for Open Multi-Agent Systems". This file explains the propositions in the demo, what files are in this archive, how to setup an eclipse project to modify the demo, and how to use the demo.

Propositions

* CarlFraud: "Carl is a fraud"
* Alice/BobPaid: "Alice/Bob paid Carl"
* PackageSentAlice/Bob: "The package that Alice/Bob ordered was sent to Alice/Bob by Carl"
* Alice/BobWaited: "Alice/Bob waited two weeks for the package to arrive"
* PackageAlice/BobDelivered: "The package of Alice/Bob was delivered"
* CarlSentPassportToAlice/Bob: "Carl send a picture of a passport to Alice/Bob"
* CarlSentLocationToAlice/Bob: "Carl mentioned he lived in a given location L to Alice/Bob"
* GBALocation: "The GBA says Carl lives some location L', L != L'"
* BankAccountOwner: "The bank says Carl is not the name of the bank account owner"
* LegitIntent: "Carl did not have a legitimate intent to send the ordered product"
* Tricks: "Carl performed tricks to try increase the chance of Bob and Alice paying him"
* LocationTrick: "Carl gave a false location"
* FalseIDTrick: "Carl gave a false identity"
* HighDeliverRatio: "Usually Carl delivers ordered products as promised"

Files

This archive consists of the following folders and files:

* "Developing Argumentation Dialogues for Open Multi-Agent Systems.pdf" is the demo paper
* "video.mp4" is the video of the demo, a player such as VLC media player can automatically load the video.srt subtitles
* Screenshots: this directory contains the screenshots that the demo paper refers to
* Visualizer: this directory is the visualizer for dialogue graphs. Open "visualizer.html" for the main demonstration. 
* Visualizer/js/dialogGraph.js contains the JSON dialogue graphs from the demo. This is what the dialogue reader outputs.
* 'dialogs': this directory contains two example dialogues. The 'dialogs/intake/' folder contains the locutions and invitations file that is referred to in the paper.
* The other folders and files are part of the Eclipse project

How-to run and modify

For those who are only interested in the demonstration itself, and not in the source code, one can simply open 'visualizer/visualizer.html' in a browser, which should load the described scenario from the paper. Click on agents to load their view. If something is not properly loaded, then a refresh usually does the trick (we use visjs and semantic ui for the layout, hence some minor layout issues are out of our control). Note that the intake agent has a lot of locutions that modify its argument 1 graph. When loading the intake agent's argument 1 graph, or the illocutionary force tab, then please wait a few seconds until the graph appears. In the argument 2 graph, if you click on a cluster it will open it. Double clicking on a locution will cluster all the locutions between the sender and receiver of that locution. For an explanation of the elements, see the demo paper. 

For modifying the visualizer of the demo, change 'visualizer/visualizer.html' or 'visualizer/js/P2PArgumentationDialogDemo.js'. 

To modify the Java part, first install Eclipse (www.eclipse.org). You can then import the project via import -> existing projects into workspace. Then, also use the import -> git screen to import the following two projects:

https://git.science.uu.nl/OO2APL/OO2APL.git
https://git.science.uu.nl/OO2APL/OO2APL-Capability-P2PCommunicationSession.git

These are agent modules that we use to specify the agents. 

After this the main method in demoProtocol/demoMain.java can be run. This program will read the locutions, invites and layout file and then output the visualizer/js/dialogGraph.js file. If you change the input files, then re-run the demo main method and refresh the visualizer to inspect the result. 

For any questions, please mail Bas Testerink, B.J.G.Testerink@uu.nl