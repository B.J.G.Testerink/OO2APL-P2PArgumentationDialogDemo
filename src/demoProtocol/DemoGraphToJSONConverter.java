package demoProtocol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import argumentationProtocols.graphs.Argument2Node;
import argumentationProtocols.graphs.CommitmentNode;
import argumentationProtocols.graphs.ConflictNode;
import argumentationProtocols.graphs.Edge;
import argumentationProtocols.graphs.EffectNode;
import argumentationProtocols.graphs.INode;
import argumentationProtocols.graphs.Node;
import argumentationProtocols.graphs.RuleApplicationNode;
import argumentationProtocols.graphs.SNode;
import argumentationProtocols.triggers.ArgumentationSendError;
import argumentationProtocols.triggers.BounceMessage; 
import oo2apl.agent.AgentID;
/**
 * This class converts the dialog graph from the demo protocol to a JSON object.
 * 
 * @author Bas Testerink
 */
public final class DemoGraphToJSONConverter {
	// Buffer that holds the conversion
	private final StringBuffer buffer;
	// Aux. memory
	private final Map<Object, Integer> elementIds; 
	private final List<Edge> argument1Edges;
	// The graph that is being converted
	private final DemoDialogGraph graph;
	// The information from the dialog reader which agents has what name in the input files
	private Map<AgentID, String> agentNames;
	// The amount of tabs that are appended when tab() is called
	private int tabIndex = 0;
	
	public DemoGraphToJSONConverter(final DemoDialogGraph graph){
		this.buffer = new StringBuffer();
		this.elementIds = new HashMap<>();
		this.argument1Edges = new ArrayList<>(); 
		this.graph = graph;
	}
	
	/** Convert AgentID to name. */
	private final String getName(final AgentID id){
		if(this.agentNames == null) return null;
		return this.agentNames.get(id);
	}
	
	/** Make and return the conversion. */
	public final StringBuffer toJSON(final Map<AgentID, String> agentNames){
		if(this.buffer.length() > 0) return this.buffer; // Already converted
		// Store the agent names
		this.agentNames = agentNames;
		
		// Make the conversion
		startBrace().newLine().incrementTab()
			.tab().addString("owner").colon().addString(getName(this.graph.getOwner())).comma().newLine()
			.errorToJSON()
			.tab().peersToJSON(agentNames.keySet()).comma().newLine()
			.tab().addString("argument2Graph").colon().startBrace().newLine().incrementTab() 
				.messagesToJSON().comma().newLine() 
				.responseRelationToJSON().decrementTab().newLine() 
			.tab().endBrace().comma().newLine() 
			.tab().addString("argument1Graph").colon().startBrace().newLine().incrementTab()
				.iNodesToJSON().comma().newLine() 
				.sNodesToJSON().comma().newLine() 
				.argument1EdgesToJSON().newLine().decrementTab()
			.tab().endBrace().comma().newLine() 
			.tab().addString("illocutionaryForce").colon().startBrace().newLine().incrementTab()
				.commitmentNodesToJSON().comma().newLine() 
				.commitmentEdgesToJSON().comma().newLine() 
				.illocutionaryForceToJSON().newLine().decrementTab()
			.tab().endBrace().comma().newLine()  
			.tab().addString("aif").colon().startBrace().newLine().incrementTab()
				.aifMetaOntologyToJSON().newLine().decrementTab()
			.tab().endBrace().newLine() 
		.endBrace();  
		return this.buffer;
	}

	/**Asserts a list with the agent names. */
	private final DemoGraphToJSONConverter peersToJSON(final Set<AgentID> peers){
		addString("agents").colon().startList(); 
		boolean first = true;
		for(AgentID peer : peers){
			if(!first) comma();
			first = false;
			addString(getName(peer));
		}
		endList();
		return this;
	}
	
	/**If there was an error, then it is appended to the output here. */
	private final DemoGraphToJSONConverter errorToJSON(){
		if(this.graph.getError().isPresent()){
			String type = "";
			String sender = "";
			String content = "";
			String receiver = "";
			String reason = "";
			if(this.graph.getError().get() instanceof BounceMessage){
				type = "bounced";
				final BounceMessage message = (BounceMessage) this.graph.getError().get();
				sender = getName(message.getBouncedMessage().getSender());
				content = message.getBouncedMessage().getContent().toString();
				receiver = getName(message.getBouncedMessage().getReceiver());
				reason = message.getReason().toString();
			} else if(this.graph.getError().get() instanceof ArgumentationSendError){ 
				type = "send error";
				final ArgumentationSendError message = (ArgumentationSendError) this.graph.getError().get();
				sender = getName(message.getBouncedMessage().getSender());
				content = message.getBouncedMessage().getContent().toString();
				receiver = getName(message.getBouncedMessage().getReceiver());
				reason = message.getReason().toString();
			}
			tab().addString("error").colon().startBrace().newLine().incrementTab()
			.tab().addString("type").colon().addString(type).comma().newLine()
			.tab().addString("reason").colon().addString(reason).comma().newLine()
			.tab().addString("sender").colon().addString(sender).comma().newLine()
			.tab().addString("content").colon().addString(content).comma().newLine()
			.tab().addString("receiver").colon().addString(receiver).decrementTab().newLine()
			.tab().endBrace().comma().newLine(); 
		} 
		return this;
	}
	
	/** Add all the i-nodes from the argument 1 graph. Also memorizes the edges from i-nodes to s-nodes. */
	private final DemoGraphToJSONConverter iNodesToJSON(){ 
		tab().addString("iNodes").colon().startList().newLine().incrementTab();
		// The i-nodes
		final List<INode> iNodes = this.graph.getNodesOfType(INode.class);
		boolean first = true;
		for(INode iNode : iNodes){
			if(!first) comma().newLine(); 
			final int id = this.elementIds.size();
			this.elementIds.put(iNode, id); // For the response relation
			tab().startBrace().addString("id").colon().addInt(id).comma()
			.addString("proposition").colon().addString(iNode.getText()).endBrace();  
			first = false;
			
			// Memorize the argument 1 edges
			for(Edge edge : iNode.getOutgoingEdges())
				if(edge.getEnd() instanceof SNode)
					this.argument1Edges.add(edge); 
		}
		newLine().decrementTab().tab().endList();
		return this;
	}

	/** Add all the s-nodes from the argument 1 graph. Also memorizes the edges from s-nodes to i-nodes. */
	private final DemoGraphToJSONConverter sNodesToJSON(){ 
		tab().addString("sNodes").colon().startList().newLine().incrementTab();
		// The messages that were made
		final List<SNode> sNodes = this.graph.getNodesOfType(SNode.class);
		boolean first = true;
		for(SNode sNode : sNodes){
			if(!first) comma().newLine(); 
			final int id = this.elementIds.size();
			this.elementIds.put(sNode, id); // For the response relation
			final String type = sNode instanceof RuleApplicationNode ? "RA" : (sNode instanceof ConflictNode ? "CA" : "Not a type");
			tab().startBrace().addString("id").colon().addInt(id).comma()
			.addString("type").colon().addString(type).endBrace();  
			first = false;
			
			// Memorize the argument 1 edges
			for(Edge edge : sNode.getOutgoingEdges())
				if(edge.getEnd() instanceof INode)
					this.argument1Edges.add(edge); 
		}
		newLine().decrementTab().tab().endList();
		return this;
	}
	
	/** Appends nodes that represent commitments. */
	private final DemoGraphToJSONConverter commitmentNodesToJSON(){ 
		tab().addString("commitmentNodes").colon().startList().newLine().incrementTab();
		// The messages that were made
		final List<CommitmentNode> commitmentNodes = this.graph.getNodesOfType(CommitmentNode.class);
		boolean first = true;
		for(CommitmentNode commitmentNode : commitmentNodes){
			if(!first) comma().newLine(); 
			final int id = this.elementIds.size();
			this.elementIds.put(commitmentNode, id); 
			tab().startBrace().addString("id").colon().addInt(id).comma() 
			.addString("owner").colon().addString(getName(commitmentNode.getCommittedAgent()))
			.endBrace();  
			first = false; 
		}
		newLine().decrementTab().tab().endList();
		
		return this;
	}
	
	/** Adds the edges from commitment nodes to other commitment nodes (commitments are represented as 
	 * linked lists within the graph) and finally to some i-node that is the subject of the commitment. */
	private final DemoGraphToJSONConverter commitmentEdgesToJSON(){ 
		tab().addString("commitmentEdges").colon().startList().newLine().incrementTab(); 
		final List<CommitmentNode> commitmentNodes = this.graph.getNodesOfType(CommitmentNode.class);
		boolean first = true;
		for(CommitmentNode commitmentNode : commitmentNodes){
			if(!first) comma().newLine(); 
			final int id = this.elementIds.size();
			final Edge edge = commitmentNode.getOutgoingEdges().get(0);
			this.elementIds.put(edge, id); 
			tab().startBrace().addString("id").colon().addInt(id).comma() 
			.addString("start").colon().addInt(this.elementIds.get(edge.getStart())).comma()
			.addString("end").colon().addInt(this.elementIds.get(edge.getEnd())).endBrace();  
			first = false; 
		}
		newLine().decrementTab().tab().endList();
		return this;
	} 
	
	/**Adds all the edges between i-nodes and s-nodes.*/
	private final DemoGraphToJSONConverter argument1EdgesToJSON(){ 
		// edges among i and s nodes
		tab().addString("argument1Edges").colon().startList().newLine().incrementTab(); 
		boolean first = true;
		for(Edge edge : this.argument1Edges){
			if(!first) comma().newLine(); 
			int id = this.elementIds.size();
			this.elementIds.put(edge, id);
			tab().startBrace().addString("id").colon().addInt(id).comma()
			.addString("start").colon().addInt(this.elementIds.get(edge.getStart())).comma()
			.addString("end").colon().addInt(this.elementIds.get(edge.getEnd())).endBrace();  
			first = false;
		}
		newLine().decrementTab().tab().endList();
		return this;
	}
	
	/**Appends the AIF ontology as a graph representation.*/
	private final DemoGraphToJSONConverter aifMetaOntologyToJSON(){ 
		// The relevant AIF ontology
		tab().addString("nodes").colon().startList().newLine().incrementTab();
		
		// Upper ontology
		addAIFNode("aif:inode", "Information node").comma().newLine();
		addAIFNode("aif:snode", "Scheme application node").comma().newLine();
		addAIFNode("aif:preference", "Preference node").comma().newLine();
		addAIFNode("aif:rule", "Rule application node").comma().newLine();
		addAIFNode("aif:conflict", "Conflict node").comma().newLine(); 
		
		// Forms ontology		
		addAIFNode("aif:form", "Form").comma().newLine();
		addAIFNode("aif:premiseDescription", "Argument premise description").comma().newLine();
		addAIFNode("aif:conclusionDescription", "Argument conclusion description").comma().newLine();
		addAIFNode("aif:presumptionDescription", "Argument presumption description").comma().newLine();  
		
 
		addAIFNode("aif:scheme", "Scheme").comma().newLine();  
		addAIFNode("aif:preferenceScheme", "Preference scheme").comma().newLine();  
		addAIFNode("aif:conflictScheme", "Conflict scheme").comma().newLine();  
		addAIFNode("aif:ruleScheme", "Rule scheme").comma().newLine();  
		addAIFNode("aif:deductiveRuleScheme", "Deductive rule scheme").comma().newLine();  
		addAIFNode("aif:inductiveRuleScheme", "Inductive rule scheme").comma().newLine();  
		addAIFNode("aif:presumptiveRuleScheme", "Presumptive rule scheme").comma().newLine();  
		  
		// Schemes ontology
		addAIFNode("schemes:negation", "Conflict due to negation (p vs. not p)").comma().newLine(); 
		addAIFNode("schemes:negationConflicted", "A").comma().newLine(); 
		addAIFNode("schemes:negationConflicting", "Not A").comma().newLine();  
		addAIFNode("schemes:defeasible", "Inference by defeasible rule").comma().newLine();   
		addAIFNode("schemes:defeasibleMinorPremise", "A").comma().newLine();   
		addAIFNode("schemes:defeasibleMajorPremise", "A => B").comma().newLine();   
		addAIFNode("schemes:defeasibleConclusion", "B");   
		newLine().decrementTab().tab().endList().comma().newLine();
		
		tab().addString("edges").colon().startList().newLine().incrementTab();
		addEdgeJSON("aif:preference","aif:snode").comma().newLine(); 
		addEdgeJSON("aif:rule","aif:snode").comma().newLine(); 
		addEdgeJSON("aif:conflict","aif:snode").comma().newLine(); 
		addEdgeJSON("aif:premiseDescription","aif:form").comma().newLine(); 
		addEdgeJSON("aif:conclusionDescription","aif:form").comma().newLine(); 
		addEdgeJSON("aif:presumptionDescription","aif:form").comma().newLine(); 
		addEdgeJSON("aif:scheme","aif:form").comma().newLine(); 
		addEdgeJSON("aif:preferenceScheme","aif:scheme").comma().newLine(); 
		addEdgeJSON("aif:conflictScheme","aif:scheme").comma().newLine(); 
		addEdgeJSON("aif:ruleScheme","aif:scheme").comma().newLine(); 
		addEdgeJSON("aif:deductiveRuleScheme","aif:ruleScheme").comma().newLine(); 
		addEdgeJSON("aif:inductiveRuleScheme","aif:ruleScheme").comma().newLine(); 
		addEdgeJSON("aif:presumptiveRuleScheme","aif:ruleScheme").comma().newLine(); 
		addEdgeJSON("schemes:negation","aif:conflictScheme").comma().newLine(); 
		addEdgeJSON("schemes:negationConflicted","aif:premiseDescription").comma().newLine(); 
		addEdgeJSON("schemes:negationConflicting","aif:conclusionDescription").comma().newLine(); 
		addEdgeJSON("schemes:defeasible","aif:presumptiveRuleScheme").comma().newLine(); 
		addEdgeJSON("schemes:defeasibleMinorPremise","aif:premiseDescription").comma().newLine(); 
		addEdgeJSON("schemes:defeasibleMajorPremise","aif:premiseDescription").comma().newLine(); 
		addEdgeJSON("schemes:defeasibleConclusion","aif:conclusionDescription").comma().newLine(); 
		
		// Connect the i-nodes
		boolean first = true;
		for(INode inode : this.graph.getNodesOfType(INode.class)){
			if(!first) comma().newLine();
			first = false;
			addEdgeJSON(this.elementIds.get(inode),"aif:inode");
		} 
		// Connect the s-nodes
		for(SNode snode : this.graph.getNodesOfType(SNode.class)){
			if(!first) comma().newLine();
			first = false;
			int id = this.elementIds.get(snode);
			// The conflict scheme
			if(snode instanceof ConflictNode){
				addEdgeJSON(id, "aif:conflict");
				comma().newLine().addEdgeJSON(id, "schemes:negation");
				for(Edge edge : snode.getIncomingEdges()){
					comma().newLine().addEdgeJSON(this.elementIds.get(edge.getStart()), "schemes:negationConflicted");
				}
				for(Edge edge : snode.getOutgoingEdges()){
					comma().newLine().addEdgeJSON(this.elementIds.get(edge.getEnd()), "schemes:negationConflicting");
				}
			// Inference by defeasible rule application
			} else if(snode instanceof RuleApplicationNode){
				addEdgeJSON(id, "aif:rule");
				comma().newLine().addEdgeJSON(id, "schemes:defeasible");
				for(Edge edge : snode.getIncomingEdges()){
					if(edge.getStart() instanceof INode){
						comma().newLine();
						final INode iNode = (INode) edge.getStart();
						if(iNode.getText().contains("=>")){
							addEdgeJSON(this.elementIds.get(edge.getStart()), "schemes:defeasibleMajorPremise");
						} else {
							addEdgeJSON(this.elementIds.get(edge.getStart()), "schemes:defeasibleMinorPremise"); 
						}
					}
				}
				for(Edge edge : snode.getOutgoingEdges()){
					if(edge.getEnd() instanceof INode){
						comma().newLine().addEdgeJSON(this.elementIds.get(edge.getEnd()), "schemes:defeasibleConclusion");
					}
				}
			}
		}
		
		newLine().decrementTab().tab().endList();
		return this;
	}

	/** Appends {"start": startID, "end": endID}. */
	private final DemoGraphToJSONConverter addEdgeJSON(final String startID, final String endID){ 
		tab().startBrace().addString("start").colon().addString(startID).comma().addString("end").colon().addString(endID).endBrace(); 
		return this;
	}
	/** Appends {"start": startID, "end": endID}. */
	private final DemoGraphToJSONConverter addEdgeJSON(final int startID, final int endID){ 
		tab().startBrace().addString("start").colon().addInt(startID).comma().addString("end").colon().addInt(endID).endBrace(); 
		return this;
	}
	/** Appends {"start": startID, "end": endID}. */
	private final DemoGraphToJSONConverter addEdgeJSON(final int startID, final String endID){ 
		tab().startBrace().addString("start").colon().addInt(startID).comma().addString("end").colon().addString(endID).endBrace(); 
		return this;
	}

	/** Appends {"id": id, "text": text}. */
	private final DemoGraphToJSONConverter addAIFNode(final String id, final String text){
		return tab().startBrace()
			.addString("id").colon().addString(id).comma()
			.addString("text").colon().addString(text)
		.endBrace();
	} 
	
	/** Connects argument 2 nodes to lists of new nodes and edges in the argument 1 graph that were the effect of the argument 2 node. */
	private final DemoGraphToJSONConverter illocutionaryForceToJSON(){ 
		// Connection from locutions to effect nodes, which are in turn connected to i and s nodes and edges among those and commitments.
		tab().addString("effects").colon().startList().newLine().incrementTab(); 
		final List<EffectNode> effectNodes = this.graph.getNodesOfType(EffectNode.class);
		boolean first = true;
		for(EffectNode effectNode : effectNodes){
			if(!first) comma().newLine();  
			
			tab().startBrace().addString("message").colon().addInt(this.elementIds.get(effectNode.getMessage())).comma()
			.addString("newNodes").colon().startList();
			boolean firstNode = true;
			for(Node node : effectNode.getNewNodes()){
				if(!firstNode) comma(); 
				addInt(this.elementIds.get(node));
				firstNode = false;
			}
			endList().comma().addString("newEdges").colon().startList(); 
			boolean firstEdge = true;
			for(Edge edge : effectNode.getNewEdges()){
				if(!firstEdge) comma();
				addInt(this.elementIds.get(edge));
				firstEdge = false;
			}
			endList().endBrace();  
			first = false; 
		}
		newLine().decrementTab().tab().endList();
		return this;
	}

	/** Adds all the messages to the output. */
	private final DemoGraphToJSONConverter messagesToJSON(){ 
		tab().addString("messages").colon().startList().newLine().incrementTab();
		// The messages that were made
		@SuppressWarnings("rawtypes")
		final List<Argument2Node> messages = this.graph.getMessageNodes();
		boolean first = true;
		for(Argument2Node<?> message : messages){
			if(!first) comma().newLine(); 
			final int id = this.elementIds.size();
			this.elementIds.put(message, id); // For the response relation
			tab().startBrace().addString("id").colon().addInt(id).comma()
			.addString("sender").colon().addString(getName(message.getSender())).comma()
			.addString("content").colon().addString(message.getContent().toString()).comma()
			.addString("receiver").colon().addString(getName(message.getReceiver())).endBrace();  
			first = false;
		}
		newLine().decrementTab().tab().endList();
		return this;	
	}
	
	/** Connect the messages in the argument 2 graph. */
	private final DemoGraphToJSONConverter responseRelationToJSON(){ 
		tab().addString("responseRelation").colon().startList().newLine().incrementTab();
		@SuppressWarnings("rawtypes")
		final List<Argument2Node> messages = this.graph.getMessageNodes();
		boolean first = true;
		for(Argument2Node<?> message : messages){
			for(Edge edge : message.getOutgoingEdges()){
				if(edge.getEnd() instanceof Argument2Node<?>){
					if(!first) comma().newLine();
					addEdgeJSON(this.elementIds.get(message), this.elementIds.get((Argument2Node<?>)edge.getEnd())); 
					first = false;
				}
			}
			
		}
		newLine().decrementTab().tab().endList();
		return this;	
	}
	
	// Auxiliary methods for adding different common character sequences

	private final DemoGraphToJSONConverter incrementTab(){ this.tabIndex++; return this; }
	
	private final DemoGraphToJSONConverter decrementTab(){ this.tabIndex--; return this; }
	
	private final DemoGraphToJSONConverter newLine(){
		this.buffer.append("\r\n"); 
		return this;	
	}
	
	private final DemoGraphToJSONConverter tab(){
		for(int i = 0; i < this.tabIndex; i++)
			this.buffer.append('\t');
		return this;	
	}
	
	private final DemoGraphToJSONConverter comma(){
		this.buffer.append(',');
		return this;	
	}
	
	private final DemoGraphToJSONConverter startBrace(){
		this.buffer.append('{');
		return this;	
	}
	
	private final DemoGraphToJSONConverter endBrace(){
		this.buffer.append('}');
		return this;	
	}
	
	private final DemoGraphToJSONConverter startList(){
		this.buffer.append('[');
		return this;	
	}
	
	private final DemoGraphToJSONConverter endList(){
		this.buffer.append(']');
		return this;	
	}

	private final DemoGraphToJSONConverter colon(){
		this.buffer.append(':');
		return this;	
	}
	
	private final DemoGraphToJSONConverter addInt(final int i){
		this.buffer.append(i);
		return this;	
	}
	
	private final DemoGraphToJSONConverter addString(final String string){
		this.buffer.append('\"').append(string).append('\"');
		return this;	
	}  
}
