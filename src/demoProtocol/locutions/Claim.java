package demoProtocol.locutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import argumentationProtocols.Template;
import argumentationProtocols.graphs.Argument2Node;
import argumentationProtocols.graphs.DialogGraph;
import argumentationProtocols.graphs.Node;
import demoProtocol.DemoDialogGraph;
import demoProtocol.DemoProtocol;
/**
 * The claim locution is used to show the intent that the agent believes some proposition 
 * to be true. 
 * 
 * @author Bas Testerink
 */
public final class Claim  implements DemoLocution {
	private final String proposition;
	
	public Claim(final String proposition){
		this.proposition = proposition;
	} 
	
	public final String getProposition(){ return this.proposition; } 

	/** Creates the template. */
	public static final Template<Claim> produceTemplate(){
		return new Template<Claim>(
				Claim.class, 
				DemoProtocol::mandateCheck, 
				Claim::responseRelation, 
				Claim::argument1Update);
	}
  
	/** A claim creates a new i-node that represents the asserted proposition. */
	private final static void argument1Update(final DialogGraph dialogGraph, final Argument2Node<Claim> newNode){
		if(!(dialogGraph instanceof DemoDialogGraph))
			return; // Can't update non-demo graphs
		final DemoDialogGraph graph = (DemoDialogGraph) dialogGraph;
		final String proposition = newNode.getContent().getProposition();
		
		// If proposition does not exist as I-Node: add new I-Node
		final Node iNode = graph.makeAndGetINode(proposition); 
		 
		// Assert the commitment for the sender 
		DemoProtocol.ensureCommitted(graph, newNode.getSender(), iNode);
		
		// Make a mandate
		final DemoLocution why = new Why(proposition);
		final DemoLocution counterClaim = new Claim(DemoProtocol.negate(proposition));
		final DemoLocution concede = new Concede(proposition);
		graph.clearMandate(newNode.getSender(), newNode.getReceiver()); // Sender fulfilled its mandate
		graph.setMandate(newNode.getReceiver(), newNode.getSender(), new DemoLocution[]{ why, counterClaim, concede }); 
		 
	}
	
	/** A claim is a response to any past counter-claim, counter-reason (q since -p), question, and why. */
	private final static List<Node> responseRelation(final DialogGraph dialogGraph, final Argument2Node<Claim> newNode){ 
		if(dialogGraph instanceof DemoDialogGraph && newNode.getContent() instanceof DemoLocution){
			final DemoDialogGraph graph = (DemoDialogGraph) dialogGraph;
			final String proposition = newNode.getContent().getProposition();
			
			final List<Node> result = new ArrayList<>(); 
			for(Argument2Node<?> message : graph.getMessageNodes()){
				if(newNode.getSender().equals(message.getReceiver()) && newNode.getReceiver().equals(message.getSender()) && (
						DemoProtocol.messageClaimsProposition(DemoProtocol.negate(proposition), message) ||
						DemoProtocol.messageIsQuestionAbout(proposition, message))
						)   
					result.add(message); 
			}
			return result; 
		}
		// Otherwise not a direct response
		return Collections.emptyList();
	}
	
	public final boolean equals(final Object object){
		if(object instanceof Claim){
			return ((Claim)object).getProposition().equals(this.proposition);
		} else return false;
	}
	
	public final String toString(){
		return "claim: "+this.proposition;
	}
}
