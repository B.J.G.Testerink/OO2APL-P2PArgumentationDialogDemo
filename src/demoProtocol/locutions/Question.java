package demoProtocol.locutions;

import java.util.Collections;
import java.util.List;
 
import argumentationProtocols.Template;
import argumentationProtocols.graphs.Argument2Node;
import argumentationProtocols.graphs.DialogGraph;
import argumentationProtocols.graphs.Node;
import demoProtocol.DemoDialogGraph;
import demoProtocol.DemoProtocol;
/**
 * A question is a dialog starter to trigger a peer into answering what it thinks 
 * of the proposition of the question.
 * 
 * @author Bas Testerink
 */
public final class Question implements DemoLocution {
	private final String proposition;
	
	public Question(final String proposition){
		this.proposition = proposition;
	}
	
	public final String getProposition(){
		return this.proposition;
	}

	/** Creates the template. */
	public static final Template<Question> produceTemplate(){
		return new Template<Question>(
				Question.class, 
				DemoProtocol::mandateCheck, 
				Question::responseRelation, 
				Question::argument1Update);
	}
 
	/** A question only changes the mandate, which is now that the receiving agent has to make a 
	 * claim, counterclaim or retract. */
	private final static void argument1Update(final DialogGraph dialogGraph, final Argument2Node<Question> newNode){
		if(!(dialogGraph instanceof DemoDialogGraph))
			return; // Can't update non-demo graphs
		final DemoDialogGraph graph = (DemoDialogGraph) dialogGraph;
		final String proposition = newNode.getContent().getProposition();
		
		// Make a mandate 
		graph.clearMandate(newNode.getSender(), newNode.getReceiver()); // Sender fulfilled its mandate
		final DemoLocution claim = new Claim(proposition);
		final DemoLocution counterClaim = new Claim(DemoProtocol.negate(proposition));
		final DemoLocution retract = new Retract(proposition);
		graph.setMandate(newNode.getReceiver(), newNode.getSender(), new DemoLocution[]{ claim, counterClaim, retract }); 
		 
	}
	
	/** A question is never a response */
	private final static List<Node> responseRelation(final DialogGraph dialogGraph, final Argument2Node<Question> newNode){ 
		//Not a direct response
		return Collections.emptyList();
	}
	
	public final boolean equals(final Object object){
		if(object instanceof Question){
			return ((Question)object).getProposition().equals(this.proposition);
		} else return false;
	}
	
	public final String toString(){
		return "question: "+this.proposition;
	}
}
