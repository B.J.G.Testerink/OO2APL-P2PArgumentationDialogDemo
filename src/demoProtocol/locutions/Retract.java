package demoProtocol.locutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
 
import argumentationProtocols.Template;
import argumentationProtocols.graphs.Argument2Node;
import argumentationProtocols.graphs.DialogGraph;
import argumentationProtocols.graphs.Node;
import demoProtocol.DemoDialogGraph;
import demoProtocol.DemoProtocol;
/**
 * An agent can use a retract locution to take back an earlier made claim.
 * 
 * @author Bas Testerink
 *
 */
public final class Retract implements DemoLocution {
	private final String proposition;
	
	public Retract(final String proposition){
		this.proposition = proposition;
	}
	
	public final String getProposition(){
		return this.proposition;
	}

	/** Creates the template. */
	public static final Template<Retract> produceTemplate(){
		return new Template<Retract>(
				Retract.class, 
				DemoProtocol::mandateCheck, 
				Retract::responseRelation, 
				Retract::argument1Update);
	}

	/** A retract ensures that the sending agent is not committed to the proposition of the locution. */
	private final static void argument1Update(final DialogGraph dialogGraph, final Argument2Node<Retract> newNode){
		if(!(dialogGraph instanceof DemoDialogGraph))
			return; // Can't update non-demo graphs
		DemoDialogGraph graph = (DemoDialogGraph) dialogGraph;
		String proposition = newNode.getContent().getProposition();
		
		// If proposition does not exist as I-Node: add new I-Node
		Node iNode = graph.makeAndGetINode(proposition); 
		
		// Assert the commitment for the sender 
		DemoProtocol.ensureNotCommitted(graph, newNode.getSender(), iNode);
		
		// Make a mandate
		graph.clearMandate(newNode.getSender(), newNode.getReceiver()); // Sender fulfilled its mandate
		graph.clearMandate(newNode.getReceiver(), newNode.getSender());
		 
	}
	
	/** A retract is a response to any past challenge or question that required the agent to defend itself. */
	private final static List<Node> responseRelation(final DialogGraph dialogGraph, final Argument2Node<Retract> newNode){ 
		if(dialogGraph instanceof DemoDialogGraph && newNode.getContent() instanceof DemoLocution){
			final DemoDialogGraph graph = (DemoDialogGraph) dialogGraph;
			final String proposition = newNode.getContent().getProposition();
			
			final List<Node> result = new ArrayList<>(); 
			for(Argument2Node<?> message : graph.getMessageNodes()){
				if(newNode.getSender().equals(message.getReceiver()) && newNode.getReceiver().equals(message.getSender()) && (
						DemoProtocol.messageIsQuestionAbout(proposition, message)||
						DemoProtocol.messageIsChallengeAbout(proposition, message)))
					result.add(message); 
			}
			return result; 
		}
		// Otherwise not a direct response
		return Collections.emptyList();
	}

	public final boolean equals(final Object object){
		if(object instanceof Retract){
			return ((Retract)object).getProposition().equals(this.proposition);
		} else return false;
	}
	
	public final String toString(){
		return "retract: "+this.proposition;
	}
}
