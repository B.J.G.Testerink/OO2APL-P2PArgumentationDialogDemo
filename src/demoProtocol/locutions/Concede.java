package demoProtocol.locutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
 
import argumentationProtocols.Template;
import argumentationProtocols.graphs.Argument2Node;
import argumentationProtocols.graphs.DialogGraph;
import argumentationProtocols.graphs.Node;
import demoProtocol.DemoDialogGraph;
import demoProtocol.DemoProtocol;
/**
 * A concede let's an agent know that the peer agrees with an earlier claim.
 * 
 * @author Bas Testerink
 *
 */
public final class Concede  implements DemoLocution {
	private final String proposition;
	
	public Concede(final String proposition){
		this.proposition = proposition;
	}
	
	public final String getProposition(){
		return this.proposition;
	} 

	/** Creates the template. */
	public static final Template<Concede> produceTemplate(){
		return new Template<Concede>( 
				Concede.class, 
				DemoProtocol::mandateCheck, 
				Concede::responseRelation, 
				Concede::argument1Update);
	}
	
	/** A concede ensures that the agent is committed to the proposition of the locution. */
	private final static void argument1Update(final DialogGraph dialogGraph, final Argument2Node<Concede> newNode){
		if(!(dialogGraph instanceof DemoDialogGraph))
			return; // Can't update non-demo graphs
		final DemoDialogGraph graph = (DemoDialogGraph) dialogGraph;
		final String proposition = newNode.getContent().getProposition();
		
		// If proposition does not exist as I-Node: add new I-Node
		final Node iNode = graph.makeAndGetINode(proposition); 
		
		// Assert the commitment for the sender 
		DemoProtocol.ensureCommitted(graph, newNode.getSender(), iNode);
		
		// Make a mandate
		graph.clearMandate(newNode.getSender(),newNode.getReceiver()); // Sender fulfilled its mandate
		graph.clearMandate(newNode.getReceiver(),newNode.getSender());
		 
	}
	
	/** A concede is a response to any past claim, or reason that asserted the proposition of the concede. */
	private final static List<Node> responseRelation(final DialogGraph dialogGraph, final Argument2Node<Concede> newNode){ 
		if(dialogGraph instanceof DemoDialogGraph && newNode.getContent() instanceof DemoLocution){
			final DemoDialogGraph graph = (DemoDialogGraph) dialogGraph;
			final String proposition = newNode.getContent().getProposition();
			
			final List<Node> result = new ArrayList<>(); 
			for(Argument2Node<?> message : graph.getMessageNodes()){
				if(newNode.getSender().equals(message.getReceiver()) && newNode.getReceiver().equals(message.getSender()) && 
						DemoProtocol.messageClaimsProposition(proposition, message))
					result.add(message); 
			}
			return result; 
		}
		// Otherwise not a direct response
		return Collections.emptyList();
	}
	
	public final boolean equals(final Object object){ 
		if(object instanceof Concede){
			return ((Concede)object).getProposition().equals(this.proposition);
		} else return false;
	}
	public final String toString(){
		return "concede: "+this.proposition;
	}
} 
