package demoProtocol.locutions;

import java.util.Collections;
import java.util.List;
 
import argumentationProtocols.Template;
import argumentationProtocols.graphs.Argument2Node;
import argumentationProtocols.graphs.DialogGraph;
import argumentationProtocols.graphs.Node;
import demoProtocol.DemoDialogGraph;
import demoProtocol.DemoProtocol;
/**
 * A skip is usually used in this protocol to pass the turn to the other agent. It is a bit 
 * like "I've got nothing to say at the moment, you go ahead".
 * 
 * @author Bas Testerink
 */
public final class Skip implements DemoLocution { 

	/** Creates the template. */
	public static final Template<Skip> produceTemplate(){
		return new Template<Skip>( 
				Skip.class, 
				DemoProtocol::mandateCheck, 
				Skip::responseRelation, 
				Skip::argument1Update);
	}
	
	/** A skip doesn't do anything but clear the mandate. */
	private final static void argument1Update(final DialogGraph dialogGraph, final Argument2Node<Skip> newNode){
		if(!(dialogGraph instanceof DemoDialogGraph))
			return; // Can't update non-demo graphs
		final DemoDialogGraph graph = (DemoDialogGraph) dialogGraph;
		
		// Clear the mandate
		graph.clearMandate(newNode.getSender(), newNode.getReceiver()); // Sender fulfilled its mandate
		graph.clearMandate(newNode.getReceiver(), newNode.getSender()); 
	}
	
	/** A skip is not a response to anything. */
	private final static List<Node> responseRelation(final DialogGraph dialogGraph, final Argument2Node<Skip> newNode){ 
		return Collections.emptyList();
	}
	
	public final String toString(){
		return "skip";
	}
}
