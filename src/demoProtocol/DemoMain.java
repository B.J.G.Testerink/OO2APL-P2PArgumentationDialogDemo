package demoProtocol; 
import java.io.IOException;

import dialogReader.DialogReader; 
/**
 * This class intiates the program by passing along the command line arguments. See the DialogReader class
 * for the actual execution of the system.
 * 
 * @author Bas Testerink
 *
 */
public final class DemoMain { 
	public final static void main(final String[] args){  
		try {
			//smallDemo();
			fraudDemo();
		} catch (IOException e) { 
			e.printStackTrace();
		}
	} 

	/** A small demo where one agents asks about P and the other responds with "since P Q". 
	 * @throws IOException */
	private final static void smallDemo() throws IOException{ 
		DialogReader.execute(
				"dialogs/small/invites.csv", 
				"dialogs/small/dialog.csv", 
				"dialogs/small/visualizationOptions.js", 
				DemoProtocol::locutionParser,
				DemoProtocol::new,
				"visualizer/js/dialogGraph.js");
	}
	
	/** A demo where different police agents discuss with each other and third parties whether some person is suspected of fraud. 
	 * @throws IOException */
	private final static void fraudDemo() throws IOException{ 
		DialogReader.execute(
				"dialogs/intake/invites.csv", 
				"dialogs/intake/locutions.csv", 
				"dialogs/intake/visualizationOptions.json", 
				DemoProtocol::locutionParser,
				DemoProtocol::new,
				"visualizer/js/dialogGraph.js");
	}
}
