package argumentationProtocols;

import java.util.List;

import argumentationProtocols.triggers.ArgumentationMessage;
import argumentationProtocols.triggers.BounceMessage;
import argumentationProtocols.triggers.BounceMessage.BounceReason;
import oo2apl.agent.AgentContextInterface;
import oo2apl.agent.PlanToAgentInterface;
import oo2apl.agent.Trigger;
import oo2apl.plan.Plan;
import oo2apl.plan.PlanExecutionError;
import oo2apl.plan.TriggerInterceptor;
import oo2apl.plan.builtin.RunOncePlan;
import p2pCommunicationCapability.CommunicationSessionId;
/**
 * 
 * The instantiate method of the interceptor immediately updates the status of the session, i.e., 
 * changes the argumentation graphs. This is a choice that breaks the norm of not executing 'plan' 
 * decision/business logic in the instantiate method. However, since multiple messages might be 
 * simultaneously in the queue, we need to update the session state, otherwise we cannot check 
 * the legality of the messages in the queue. 
 * 
 * @author Bas Testerink
 */
public final class MessageInterceptor extends TriggerInterceptor {
	private final CommunicationSessionId sessionId;
	private final StrictP2PTurnMechanism turnMechanism;
	private final List<Template<?>> templates;
	
	public MessageInterceptor(final CommunicationSessionId sessionId, 
			final StrictP2PTurnMechanism turnMechanism, final List<Template<?>> templates) {
		super(false); 
		this.sessionId = sessionId;
		this.turnMechanism = turnMechanism;
		this.templates = templates;
	}
 
	/** The interceptor only triggers for argumentation messages. When it triggers, then it will initially flag the 
	 * message to illegal, until proven otherwise. First it checks the turn mechanism, and then tries to apply the 
	 * templates. The first template that is applicable is applied and no other templates. */
	public final Plan instantiate(final Trigger trigger, final AgentContextInterface contextInterface){
		// The trigger has to be an argumentation message for the session that this interceptor is part of
		if(trigger instanceof ArgumentationMessage<?>){
			final ArgumentationMessage<?> message = (ArgumentationMessage<?>) trigger;
			if(message.getSessionId().equals(this.sessionId)){
				// By default set the message to non-legal
				message.setLegal(false); 
				
				// Check the turn status, if the message is out-of-turn, then return a plan to send the sender a 
				// notification that its message is bounced
				if(!this.turnMechanism.allowedToSendTo(message.getSender(), message.getReceiver()))
					return bouncePlan(message, BounceReason.NOT_YOUR_TURN); 
 
				// Go through the templates to see if any allows the message
				for(Template<?> template : this.templates){
					if(template.apply(message)){
						message.setLegal(true);
						// Process that you received successfully a message from the sender
						this.turnMechanism.sentMessage(message.getSender(), message.getReceiver());
						return Plan.UNINSTANTIATED;
					}
				}
				// Bounce the message if there was no applicable template
				return bouncePlan(message, BounceReason.NO_APPLICABLE_TEMPLATE);
			}
		}
		return Plan.UNINSTANTIATED;
	}

	/** Plan that notifies the sender of a bounced message that its message was bounced. */
	private final static Plan bouncePlan(final ArgumentationMessage<?> bouncedMessage, final BounceReason reason){
		return new RunOncePlan(){ 
			public void executeOnce(PlanToAgentInterface planInterface) throws PlanExecutionError {
				BounceMessage message = new BounceMessage(
						bouncedMessage.getSessionId(), 
						planInterface.getAgentID(), 
						bouncedMessage,
						reason);
				planInterface.sendMessage(bouncedMessage.getSender(), message);
			}
		};
	}
}
