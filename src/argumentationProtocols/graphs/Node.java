package argumentationProtocols.graphs;

import java.util.ArrayList;
import java.util.List;
/**
 * A simple Node class that maintains the incoming and outgoing edges.
 * 
 * @author Bas Testerink
 *
 */
public class Node {
	private final List<Edge> outgoingEdges, incomingEdges;
	
	public Node(){
		this.outgoingEdges = new ArrayList<>();
		this.incomingEdges = new ArrayList<>();
	}

	public final void addOutgoingEdge(final Edge edge){
		if(edge.getStart() == this)
			this.outgoingEdges.add(edge);
	}
	
	public final void addIncomingEdge(final Edge edge){
		if(edge.getEnd() == this)
			this.incomingEdges.add(edge);
	}
	
	/** Add an outgoing edge to the other node and add in the other node an incoming edge
	 * to this node. */
	public final Edge connectTo(final Node other){
		Edge edge = new Edge(this, other);
		other.addIncomingEdge(edge);
		this.addOutgoingEdge(edge);
		return edge;
	}
	
	public final List<Edge> getIncomingEdges(){ return this.incomingEdges; } 
	public final List<Edge> getOutgoingEdges(){ return this.outgoingEdges; }
}
