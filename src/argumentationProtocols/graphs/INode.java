package argumentationProtocols.graphs;
/**
 * An i node represents a piece of information such as propositions.
 * 
 * @author Bas Testerink
 *
 */
public class INode extends Node {
	private final String text;
	
	public INode(final String text){
		this.text = text;
	}
	
	public final String getText(){
		return this.text; 
	}
}
