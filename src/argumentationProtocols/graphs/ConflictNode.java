package argumentationProtocols.graphs;
/**
 * A conflict node tells what iNode is in conflict with what other iNode. This relation is 
 * directed to be able to capture 'contrariness'. 
 * 
 * @author Bas Testerink
 *
 */
public class ConflictNode extends SNode { 
	 private final Node conflicted, conflicting;
	 
	 public ConflictNode(final Node conflicted, final Node conflicting){
		 this.conflicted = conflicted;
		 this.conflicting = conflicting;
	 }

	public boolean equals(final Object other){
		if(other == null) return false;
		if(other == this) return true;
		if(other instanceof ConflictNode){
			ConflictNode otherNode = (ConflictNode) other;
			return otherNode.conflicted == this.conflicted &&
					otherNode.conflicting == this.conflicting; 
		}
		return false;
	}
}
