package argumentationProtocols.graphs;

import java.util.Collections;
import java.util.List;
/**
 * An effect node connects an argument 2 node (locution) to its effects in the argument 1 graph.
 * 
 * @author Bas Testerink
 */
public final class EffectNode extends Node {
	private List<Node> newNodes;
	private List<Edge> newEdges;
	private final Argument2Node<?> message;
	
	EffectNode(final Argument2Node<?> message, final List<Node> newNodes, final List<Edge> newEdges){
		this.message = message;
		this.newNodes = Collections.unmodifiableList(newNodes);
		this.newEdges = Collections.unmodifiableList(newEdges);
	}
	 
	public final Argument2Node<?> getMessage(){ return this.message; }
	public final List<Node> getNewNodes(){ return this.newNodes; }
	public final List<Edge> getNewEdges(){ return this.newEdges; }
}
