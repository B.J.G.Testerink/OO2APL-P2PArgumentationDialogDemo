package argumentationProtocols.triggers;
 
import oo2apl.agent.AgentID;
import oo2apl.agent.Trigger;
import p2pCommunicationCapability.CommunicationSessionId;
/**
 * A trigger that is automatically adopted internally in the agent when it tries to illegally 
 * send a message through the argumentation capability. Note that if the agent decides to still 
 * send the message through the regular messenger call, then the agent is capable of violating the 
 * protocol. 
 * 
 * @author Bas Testerink
 *
 */
public final class ArgumentationSendError implements Trigger {
	public static enum ErrorReason{ NOT_MY_TURN, NO_APPLICABLE_TEMPLATE }
	private final CommunicationSessionId sessionId;
	private final AgentID sender;
	private final ArgumentationMessage<?> bouncedMessage;
	private final ErrorReason reason;
	
	public ArgumentationSendError(final CommunicationSessionId sessionId, final AgentID sender, 
			final ArgumentationMessage<?> bouncedMessage, final ErrorReason reason){
		this.sessionId = sessionId;
		this.sender = sender;
		this.bouncedMessage = bouncedMessage;
		this.reason = reason;
	}
	
	public final CommunicationSessionId getSessionId(){ return this.sessionId; }
	public final AgentID getSender(){ return this.sender; }
	public final ArgumentationMessage<?> getBouncedMessage(){ return this.bouncedMessage; }
	public final ErrorReason getReason(){ return this.reason; }

}
