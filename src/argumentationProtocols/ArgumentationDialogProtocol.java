package argumentationProtocols;

import java.util.ArrayList;
import java.util.List;
import argumentationProtocols.graphs.DialogGraph;
import argumentationProtocols.triggers.ArgumentationMessage;
import argumentationProtocols.triggers.ArgumentationSendError;
import argumentationProtocols.triggers.ArgumentationSendError.ErrorReason;
import oo2apl.agent.AgentID;
import oo2apl.agent.PlanToAgentInterface;
import p2pCommunicationCapability.CommunicationSessionId;
/**
 * A protocol more or less consists of the receive and send templates that tell 
 * how to interpret locution for this protocol. The templates update the dialog 
 * graph which is also stored in the protocol instance. 
 * 
 * We store alongside the graph a turn mechanism separately. The mechanism could 
 * be stored as part of the graph but that would be unnecessarily complicating 
 * the system architecture. The specification and maintenance of turn semantics 
 * will in the future be revisited.
 * 
 * Finally, we add a message interceptor to the agent that automatically checks 
 * all the receive templates whenever an argumentation messages arrives. 
 * 
 * @author Bas Testerink
 *
 * @param <Locution>
 */
public abstract class ArgumentationDialogProtocol<Locution> {
	private final CommunicationSessionId sessionId;
	private final List<Template<?>> receiveTemplates, sendTemplates;
	private StrictP2PTurnMechanism turnMechanism;
	private DialogGraph dialogGraph; 
	private MessageInterceptor interceptor;
	
	public ArgumentationDialogProtocol(final CommunicationSessionId sessionId){
		this.sessionId = sessionId;
		this.receiveTemplates = new ArrayList<>();
		this.sendTemplates = new ArrayList<>();
	}  
 
	/** Make this protocol ready to use by making the dialog graph, setting 
	 * this graph in the templates, making the turn mechanism and finally 
	 * adopting the message interceptor. */
	public final void finalize(final AgentID owner){
		this.dialogGraph = makeDialogGraph(owner);
		for(Template<?> template : this.receiveTemplates)
			template.setDialogGraph(this.dialogGraph);
		for(Template<?> template : this.sendTemplates)
			template.setDialogGraph(this.dialogGraph); 
		this.turnMechanism = new StrictP2PTurnMechanism(owner);
		this.interceptor = new MessageInterceptor(this.sessionId, this.turnMechanism, this.receiveTemplates);
	}
	
	/** Finalize the protocol and adopt the message interceptor that checks incoming messages. This method 
	 * ought to be called before the agent responds to an invitation to join an argumentation session. */
	public final static void initiateArgumentationDialog(
			final PlanToAgentInterface planInterface,  
			final ArgumentationDialogProtocol<?> protocol){
		protocol.finalize(planInterface.getAgentID());  
		planInterface.adoptMessageInterceptor(protocol.getMessageInterceptor()); 
	}
	
	/** An argumentation agent can use this send method to send an argumentation message which is then immediately 
	 * checked against the protocol. If the message is illegal, then the agent will adopt an internal trigger 
	 * (an <code>ArgumentationSendError</code>). */
	public final static boolean send(final PlanToAgentInterface planInterface, final ArgumentationDialogProtocol<?> protocol, final ArgumentationMessage<?> message){
		List<Template<?>> sendTemplates = protocol.getSendTemplates();
		
		// First check if the turn mechanism allows the agent's move
		if(!protocol.getTurnMechanism().allowedToSendTo(planInterface.getAgentID(), message.getReceiver())){ 
			planInterface.addInternalTrigger(new ArgumentationSendError(protocol.getSessionId(),planInterface.getAgentID(),message,ErrorReason.NOT_MY_TURN));
			return false;
		}
		
		// Check if there is a send template that allows the agent's move
		boolean success = false; 
		for(Template<?> template : sendTemplates){
			if(template.apply(message)){
				success = true; 
				// Successfully sent the message
				protocol.getTurnMechanism().sentMessage(planInterface.getAgentID(), message.getReceiver());  
				planInterface.sendMessage(message.getReceiver(), message); 
				break;
			}
		}
		
		// If there were not applicable templates, then the agent notifies itself that there was an error
		if(!success) 
			planInterface.addInternalTrigger(new ArgumentationSendError(protocol.getSessionId(),planInterface.getAgentID(),message,ErrorReason.NO_APPLICABLE_TEMPLATE));
		return success; 
	} 
	
	/** Make a new dialog graph with the given agent as the owner. */
	protected abstract DialogGraph makeDialogGraph(final AgentID owner);
	
	/** Add a template that filters incoming messages. */
	public final void addReceiveTemplate(final Template<?> template){
		this.receiveTemplates.add(template); 
	}
	
	/** Add a template that filters outgoing messages. */
	public final void addSendTemplate(final Template<?> template){
		this.sendTemplates.add(template);
	}
 
	// Getters
	public final MessageInterceptor getMessageInterceptor(){ return this.interceptor; }
	public final DialogGraph getDialogGraph(){ return this.dialogGraph; }
	public final StrictP2PTurnMechanism getTurnMechanism(){ return this.turnMechanism; }
	public final List<Template<?>> getSendTemplates(){ return this.sendTemplates; }
	public final CommunicationSessionId getSessionId(){ return this.sessionId; }
	
}
