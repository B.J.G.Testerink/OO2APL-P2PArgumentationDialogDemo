package dialogReader;
/**
 * 
 * These entries are used to create a queue that represents the locution script.
 * 
 * @author Bas Testerink
 *
 * @param <Locution>
 */
public final class DialogQueueEntry<Locution> {
	private final String sender, receiver;
	private final Locution locution;
	
	public DialogQueueEntry(final String sender, final String receiver, final Locution locution){
		this.sender = sender;
		this.receiver = receiver;
		this.locution = locution;
	}
	
	public final String getSender(){ return this.sender; }
	public final String getReceiver(){ return this.receiver; }
	public final Locution getLocution(){ return this.locution; }
}
