package dialogReader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.function.Consumer;
import java.util.function.Function;

import argumentationProtocols.ArgumentationDialogProtocol;
import dialogReader.readerAgent.ReaderAgent;
import dialogReader.readerAgent.triggers.MessagePrompt;
import dialogReader.readerAgent.triggers.PublishPrompt;
import oo2apl.agent.AgentID;
import oo2apl.agent.ExternalProcessToAgentInterface; 
import oo2apl.defaults.messenger.DefaultMessenger;
import oo2apl.platform.AdminToPlatformInterface;
import oo2apl.platform.Platform;
import p2pCommunicationCapability.CommunicationSessionId;
import p2pCommunicationCapability.triggers.SessionOrganizeTrigger;

/**
 * A DialogReader can read a dialog log/prewritten script and re-enact it. It does so by creating 
 * agents that follow the log's prescribes moves (they do not execute a decision logic themselves). 
 * By executing the moves, the protocol system automatically builds a dialog graph for each agent. 
 * When the end of the script is reached, or when a move was illegal, the dialog halts and the 
 * graphs are outputted as JSON objects. Every graph is decomposed in subgraphs such as a graph 
 * that shows the coherence among locutions according to the protocol. This software comes with 
 * a simple webinterface that allows a user to visualize this graph.
 * 
 * A script consists of two files; an invitation file and a locutions file. The invitations are 
 * pairs of agent names. First name in the file  will be the organizer of the dialog session. 
 * From then on, when an agent is invited to join the session or is the organizer, it will look 
 * at all the pairs where it is itself the first agent and then extend the invitation to the second 
 * agent. For instance:
 * 
 * Alice,Bob
 * Carl,Dean
 * Alice,Earl
 * Earl,Carl
 * Dean,Alice
 * 
 * Means that Alice organizes the session and invites Bob and Earl. Earl will extend the invitation 
 * to Carl, and Carl extends it to Dean. Then, Dean extends the invitation to Alice. Since Alice is 
 * already in the session, she will not re-invite Bob and Earl. But she is now notified that in this 
 * session Dean is a peer of her's and she has to consider that he may send her messages (and the 
 * other way around). 
 * 
 * The second file is the locutions file. This file consists of a list of locutions:
 * Alice,hello,Bob
 * Bob,hi,Alice
 * 
 * In this script Alice sends the 'hello' locution to Bob, who responds with 'hi'. 
 * 
 * @author Bas Testerink
 *
 */
public final class  DialogReader<Locution> {
	// Agents get unique ID's, this map converts the agent's ID from OO2APL to its name in the dialog
	private final Map<AgentID, String> idToName;
	// This map converts the name of an agent in the dialog to its external interface, which allows us to 
	// send prompts to the agent that trigger behavior
	private final Map<String, ExternalProcessToAgentInterface> nameToInterface;
	// The resulting script from parsing the locution file
	private final Queue<DialogQueueEntry<Locution>> dialogQueue;  
	// The parser for parsing the locutions in the locution file
	private final Function<String, Locution> locutionParser;
	// Factory that produces protocols upon given a new session id
	private final Function<CommunicationSessionId, ArgumentationDialogProtocol<Locution>> protocolFactory;
	// The resulting invitation structure from reading the invitations file
	private final Map<String, List<String>> invitations; 
	// The organizer of the communication session, is set when the invitation file is read
	private String organizer = null;
	// Buffer that stores the output
	private final StringBuffer output;
	// Path to the output file
	private final String outputFilePath;
	// Path to the visualization file
	private final String visualizationOptionsFilePath;
	// The number of agents that have not finished outputting their dialog graphs
	private int nrOfAgentsNotDone;
	
	/** Read the files and let the agents execute the script. */
	public static final <Locution> void execute(
			final String invitationsFilePath, 
			final String locutionsFilePath, 
			final String visualizationOptionsFilePath, 
			final Function<String, Locution> locutionParser,
			final Function<CommunicationSessionId, ArgumentationDialogProtocol<Locution>> protocolFactory,
			final String outputFilePath) throws IOException{ 
		final DialogReader<Locution> reader = new DialogReader<>(invitationsFilePath, locutionsFilePath, visualizationOptionsFilePath, locutionParser, protocolFactory, outputFilePath);
		reader.start();
	}
	
	/**
	 * Constructor, immediately reads the provided files.
	 * @param invitationsFilePath The file that contains the invitations.
	 * @param locutionsFilePath The file that contains the locutions script.
	 * @throws IOException Thrown when something went wrong during reading the files. This is not a parse error.
	 */
	private DialogReader(
			final String invitationsFilePath, 
			final String locutionsFilePath, 
			final String visualizationOptionsFilePath, 
			final Function<String, Locution> locutionParser,
			final Function<CommunicationSessionId, ArgumentationDialogProtocol<Locution>> protocolFactory,
			final String outputFilePath) throws IOException{ 
		
		// Initialize the data storage
		this.idToName = new HashMap<>();
		this.nameToInterface = new HashMap<>();
		this.dialogQueue = new LinkedList<>();
		this.locutionParser = locutionParser;
		this.protocolFactory = protocolFactory;
		this.invitations = new HashMap<>(); 
		this.output = new StringBuffer();
		this.outputFilePath = outputFilePath;
		this.visualizationOptionsFilePath = visualizationOptionsFilePath;
		this.nrOfAgentsNotDone = 0;
		
		// Read the files
		readFileLineByLine(invitationsFilePath, this::parseInvitationLine);
		readFileLineByLine(locutionsFilePath, this::parseLocutionLine);  
	} 
	
	/**
	 * Parses a locution line of the form "Sender,Locution,Receiver". Will do nothing if the 
	 * line is not conform this format.
	 * @param line The line to be parsed.
	 */
	private final void parseLocutionLine(final String line){
		// There must be a line
		if(line == null) return; 

		// Each locution line consists of exactly 3 elements Sender,Locution,Receiver
		final String[] split = line.split(",");
		if(split.length != 3) return; 
		
		// Remember the encountered names, the interfaces will be created later
		this.nameToInterface.put(split[0], null); 
		this.nameToInterface.put(split[2], null);
		
		// Parse the locution (done by the protocol) 
		this.dialogQueue.add(new DialogQueueEntry<Locution>(split[0], split[2], this.locutionParser.apply(split[1])));
	}

	/**
	 * Parses an invitation line of the form "Inviter,Invitee". Will do nothing if the 
	 * line is not conform this format.
	 * @param line The line to be parsed.
	 */
	private final void parseInvitationLine(final String line){
		// Check if the line can be parsed
		if(line == null || // There must be a line to begin with
				!line.contains(",") || // There must be a comma to indicate where the first name ends
				line.indexOf(',') != line.lastIndexOf(',')) // There must be exactly one pair, and not for instance Alice,Bob,Carl 
			return; // Simply break off the operation
		
		// Split the line in the first and second name
		final String[] split = line.split(",");  
		
		// Remember the encountered names, the interfaces will be created later
		this.nameToInterface.put(split[0], null); 
		this.nameToInterface.put(split[1], null);
		
		// Remember the organizer, it is always the first name in the invites list
		if(this.organizer == null)
			this.organizer = split[0];
		
		// Add the second name to the list of to-be-invited peers of the first name
		List<String> peersToInvite = this.invitations.get(split[0]);
		if(peersToInvite == null){ // This is the first peer to invite, so create the list
			peersToInvite = new ArrayList<>();
			this.invitations.put(split[0], peersToInvite);
		}
		peersToInvite.add(split[1]);
	}
	
	/**
	 * Read the given file line by line.
	 * @param filePath The path to the file to be read.
	 * @param lineOperation The operation to perform on each line.
	 * @throws IOException Thrown when the file did not exist.
	 */
	private final static void readFileLineByLine(final String filePath, final Consumer<String> lineOperation) throws IOException { 
		final BufferedReader reader = new BufferedReader(new FileReader(filePath));
		reader.lines().forEach(lineOperation);
		reader.close();
	}
	
	/** Make the agent system. */
	private final void start(){
		// Build the platform
		final AdminToPlatformInterface platform = Platform.newPlatform(1, new DefaultMessenger());
		
		// Make the agents
		for(String name : new HashSet<>(this.nameToInterface.keySet())){ // New set because we modify the map
			// Make the agent
			final ExternalProcessToAgentInterface agent =  platform.newAgent(new ReaderAgent<Locution>(this));
			this.nameToInterface.put(name, agent);
			this.idToName.put(agent.getAgentID(), name);
			this.nrOfAgentsNotDone++;
		} 
		
		// Initiate the communication session 
		final ExternalProcessToAgentInterface organizerAgent = this.nameToInterface.get(this.organizer);
		organizerAgent.addExternalTrigger(new SessionOrganizeTrigger(getPeersToInvite(organizerAgent.getAgentID())));    
	}
	
	/**
	 * Given the name of an agent, returns the invitations that it must extend.
	 * @param agentThatInvites
	 * @return
	 */
	public final AgentID[] getPeersToInvite(final AgentID agentThatInvites){
		// No peers to invite
		if(agentThatInvites == null) return new AgentID[0]; 
		
		// This operation can be done concurrently with other agents that have finished inviting agents
		synchronized(this.invitations){ 
			final List<String> peerNames = this.invitations.get(this.idToName.get(agentThatInvites));
			if(peerNames == null || peerNames.size() == 0) return new AgentID[0]; 
			
			// Return the mapping from names to id's otherwise
			return peerNames.stream().map(this::asAgentID).toArray(AgentID[]::new);
		}
	}
	
	/**
	 * Removes the accepting agent form the inviter's list of peers that it ought to extend 
	 * the invitation to. When there are no more invitations to be processed, then the 
	 * first agent is prompted to start executing the locutions script.
	 * @param acceptingAgent
	 * @param inviter
	 */
	public final void acceptedInvitation(final AgentID acceptingAgent, final AgentID inviter){
		if(acceptingAgent == null || inviter == null) return;
		
		// This operation can be done concurrently with other agents checking who to invite
		synchronized(this.invitations){
			// Get and check the names
			final String nameAcceptingAgent = this.idToName.get(acceptingAgent);
			final String nameInvitingAgent = this.idToName.get(inviter);
			if(nameAcceptingAgent == null || nameInvitingAgent == null) return;
			
			// Remove from the list
			final List<String> toBeAccepted = this.invitations.get(nameInvitingAgent);
			if(toBeAccepted != null && toBeAccepted.remove(nameAcceptingAgent)){ 
				if(toBeAccepted.isEmpty()){
					// Remove the inviting agent all together, its peers have all accepted the invitation
					this.invitations.remove(nameInvitingAgent);
					// If all invitations are done, then start the argumentation dialog
					if(this.invitations.isEmpty())
						promptNextAgent();  
				}
			}
		}
	}
	
	/** Prompt the next agent in the queue to send its assigned message. */
	public final void promptNextAgent(){
		// If the dialog is done, then publish the dialog graphs
		if(this.dialogQueue.isEmpty()){ 
			System.out.println("done, publish");
			broadcastPublishPrompt();
		} else {
			// Otherwise get the next entry and send its message away
			final DialogQueueEntry<Locution> entry = this.dialogQueue.remove();
			final ExternalProcessToAgentInterface agent = this.nameToInterface.get(entry.getSender());
			if(agent == null) return; // TODO: handle error
			agent.addExternalTrigger(new MessagePrompt<>(entry.getLocution(), asAgentID(entry.getReceiver())));
		}
	}
	
	/**
	 * Add the dialog graph to the output.
	 * @param agent
	 * @param graph
	 */
	public final void publishGraph(final StringBuffer graph){
		synchronized(this.output){
			// Append the result
			this.output.append(",\r\n").append(graph);
			this.nrOfAgentsNotDone--;
			// Everybody is done! output to file
			if(this.nrOfAgentsNotDone == 0){
				final StringBuffer buffer = new StringBuffer();
				buffer.append("var dialogGraphs = {\r\n");
				this.output.setCharAt(0, ' '); // Remove the first comma
				buffer.append(this.output);
				buffer.append("};\r\n");
				// Write to file and exit
				try (final PrintWriter writer = new PrintWriter(this.outputFilePath)){
					// Add the visualization options
					readFileLineByLine(this.visualizationOptionsFilePath, line -> {buffer.append(line).append("\r\n");});
					writer.print(buffer.toString()); 
					writer.flush();
					writer.close();
					System.exit(0);
				} catch (IOException e) { 
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Convert the name of an agent to its ID if it exists (otherwise returns null).
	 * @param name
	 * @return
	 */
	private final AgentID asAgentID(final String name){
		final ExternalProcessToAgentInterface agentInterface = this.nameToInterface.get(name); 
		return agentInterface == null ? null : agentInterface.getAgentID();
	}
	
	/**
	 * Convert the ID of an agent to its name.
	 * @param id
	 * @return
	 */
	public final String asName(final AgentID id){
		return this.idToName.get(id);
	}
	
	/** Notify the reader that an error occurred while executing the locution script. This 
	 * always means that the next locution was illegal.*/
	public final void error(){
		System.out.println("error, publish"); 
		broadcastPublishPrompt();
	}
	
	/** Broadcast a signal to all agents that prompts them to output their dialog graph. */
	private final void broadcastPublishPrompt(){ 
		for(ExternalProcessToAgentInterface agent : this.nameToInterface.values())
			agent.addExternalTrigger(PublishPrompt.getInstance());
	}
	  
	/** Create a new protocol instance. */
	public final ArgumentationDialogProtocol<Locution> produceProtocol(final CommunicationSessionId sessionId){
		return this.protocolFactory.apply(sessionId);
	}
}
