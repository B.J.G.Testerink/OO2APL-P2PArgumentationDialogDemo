package dialogReader.readerAgent;
 
import java.util.HashMap;
import java.util.Map;

import argumentationProtocols.ArgumentationDialogProtocol; 
import dialogReader.DialogReader;
import oo2apl.agent.AgentID;
import oo2apl.agent.Context;
import oo2apl.agent.PlanToAgentInterface;
import oo2apl.agent.Trigger;
import p2pCommunicationCapability.CommunicationSessionId;
import p2pCommunicationCapability.triggers.InvitePeerTrigger;
/**
 * In OO2APL we store in the context all functionalities that agents need in order to execute their 
 * decision logic and actions. In this case we only store the dialog reader of the session and the 
 * protocol so that the agent can check whether it is the next agent to send a locution. 
 * 
 * @author Bas Testerink
 *
 * @param <Locution>
 */
public final class ReaderAgentContext<Locution> implements Context {
	private final DialogReader<Locution> reader;
	private  ArgumentationDialogProtocol<Locution> protocol = null; 
	
	public ReaderAgentContext(final DialogReader<Locution> reader){
		this.reader = reader; 
	} 
	
	/** Turn the dialog graph into JSON and send to the dialog reader. */
	public final void publish(){ 
		final Map<AgentID, String> peerNames = new HashMap<>();
		final String ownerName =  this.reader.asName(this.protocol.getDialogGraph().getOwner());
		peerNames.put(this.protocol.getDialogGraph().getOwner(), ownerName);
		for(AgentID peer : this.protocol.getTurnMechanism().getAllConnectedPeers())
			peerNames.put(peer, this.reader.asName(peer));
		this.reader.publishGraph((new StringBuffer()).append("\""+ownerName+"\":").append(this.protocol.getDialogGraph().toJSON(peerNames)));
	}
	
	/** Let the dialog reader know that an error occurred. */
	public final void error(final Trigger errorTrigger){
		this.protocol.getDialogGraph().setError(errorTrigger);
		this.reader.error();
	}
	
	/** Prompt the next agent in the script to send a locution. */
	public final void promptNextAgent(){
		this.reader.promptNextAgent();
	}
	
	/** Add a peer (to the turntaking mechanism).  */
	public final void addPeer(final AgentID peer, final boolean invitedMe){
		this.protocol.getTurnMechanism().addPeer(peer, invitedMe);
	}
	
	/** Set the protocol. */
	public final void setProtocol(final ArgumentationDialogProtocol<Locution> protocol){
		this.protocol = protocol;
	}
	
	/** Get the agents for which you are required to extend the session invitation. */
	public final AgentID[] getPeersToInvite(){
		return this.reader.getPeersToInvite(this.protocol.getDialogGraph().getOwner());
	}
	
	/** Let the reader know that one of the peers accepted your invitation to join the session. */
	public final void acceptedInvitation(final AgentID peer){
		this.reader.acceptedInvitation(peer, this.protocol.getDialogGraph().getOwner());
	}
	
	/** Get the protocol. */
	public final ArgumentationDialogProtocol<Locution> getProtocol(){
		return this.protocol;
	} 
	
	/** Create a protocol if this call is made for the first time and extend the invitation to the appropriate peers. */
	public final void initiateDialog(final PlanToAgentInterface planInterface, final CommunicationSessionId sessionId){
		if(this.protocol == null){
			// Make the protocol
			this.protocol = this.reader.produceProtocol(sessionId);
			ArgumentationDialogProtocol.initiateArgumentationDialog(planInterface, this.protocol);
			
			// Spread the session
			for(AgentID peer : getPeersToInvite())
				planInterface.addInternalTrigger(new InvitePeerTrigger(peer, sessionId)); 
		} 
	}
}
