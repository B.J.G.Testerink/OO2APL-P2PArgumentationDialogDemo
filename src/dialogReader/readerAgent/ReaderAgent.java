package dialogReader.readerAgent;

import java.util.function.BiConsumer;
import argumentationProtocols.ArgumentationDialogProtocol;
import argumentationProtocols.triggers.ArgumentationMessage;
import argumentationProtocols.triggers.ArgumentationSendError;
import argumentationProtocols.triggers.BounceMessage;
import dialogReader.DialogReader;
import dialogReader.readerAgent.triggers.MessagePrompt;
import dialogReader.readerAgent.triggers.PublishPrompt;
import oo2apl.agent.AgentBuilder;
import oo2apl.agent.AgentID;
import oo2apl.agent.PlanToAgentInterface;
import oo2apl.agent.Trigger;
import oo2apl.plan.builtin.FunctionalPlanSchemeInterface;
import oo2apl.plan.builtin.SubPlanInterface;
import p2pCommunicationCapability.CommunicationCapability;
import p2pCommunicationCapability.CommunicationSessionId;
import p2pCommunicationCapability.triggers.SessionInvitation;
import p2pCommunicationCapability.triggers.SessionOrganizeTrigger;
/**
 * A reader agent can receive triggers that prompt it to publish a dialog graph or send a 
 * locution to another reader agent. The communication session makes use of the P2P capability 
 * of OO2APL in order to manage the sessions and invitations.
 * 
 * @author Bas Testerink
 */
public final class ReaderAgent<Locution> extends AgentBuilder { 
	
	public ReaderAgent(final DialogReader<Locution> dialogReader){		
		addContext(new ReaderAgentContext<>(dialogReader));
		
		// Setup the P2P communication capability
		include(new CommunicationCapability(
				ReaderAgent::onOrganize, 
				ReaderAgent::onInvited, 
				ReaderAgent::onAccept, 
				ReaderAgent::onReject, 
				ReaderAgent::onQuit));

		// Plan scheme to handle the invitation of peers, when so prompted
		addExternalTriggerPlanScheme(onTriggerTypeScheme(SessionOrganizeTrigger.class, ReaderAgent::invitePeers));
		// Send the next message, when so prompted 
		addExternalTriggerPlanScheme(onTriggerTypeScheme(MessagePrompt.class, ReaderAgent::handleMessagePrompt));
		// Send the next message, when so prompted 
		addExternalTriggerPlanScheme(onTriggerTypeScheme(PublishPrompt.class, ReaderAgent::publishDialogGraph));
		
		// Plan scheme for argumentation strategy 
		addMessagePlanScheme(onTriggerTypeScheme(ArgumentationMessage.class, ReaderAgent::handleReceivedArgumentationMessage));
		
		// Handling errors
		addInternalTriggerPlanScheme(onTriggerTypeScheme(ArgumentationSendError.class, ReaderAgent::handleSendError)); // Your message couldn't be sent 
		addMessagePlanScheme(onTriggerTypeScheme(BounceMessage.class, ReaderAgent::handleBouncedMessage)); // Your message couldn't be received
	}
	
	/** Create a plan scheme that triggers for each trigger that is an instanceof the given class and executes the given BiConsumer. */
	private static final FunctionalPlanSchemeInterface onTriggerTypeScheme(final Class<? extends Trigger> klass, final BiConsumer<Trigger, PlanToAgentInterface> plan){
		return (trigger, contextInterface) -> { 
			if(klass.isInstance(trigger)) return (planInterface)->{ plan.accept(trigger, planInterface); };
			else return SubPlanInterface.UNINSTANTIATED;
		};
	}
	
	/** Adopts a session organize trigger (from the dialog reader) as an internal reader, thus activating the P2P communication capability. */
	private static final void invitePeers(final Trigger rawTrigger, final PlanToAgentInterface planInterface){
		planInterface.addInternalTrigger(rawTrigger); 
	} 

	/** Publishes the dialog graph as a JSON object. */
	private static final void publishDialogGraph(final Trigger rawTrigger, final PlanToAgentInterface planInterface){
		planInterface.getContext(ReaderAgentContext.class).publish();   
	}  

	/** Dialog was illegal. */
	private static final void handleSendError(final Trigger rawTrigger, final PlanToAgentInterface planInterface){
		System.out.println("There was a send error.");
		planInterface.getContext(ReaderAgentContext.class).error(rawTrigger);   
	}
	
	/** Dialog was illegal. */
	private static final void handleBouncedMessage(final Trigger rawTrigger, final PlanToAgentInterface planInterface){
		System.out.println("There was a receive error.");
		planInterface.getContext(ReaderAgentContext.class).error(rawTrigger);   
	}
	 
	/** Send a locution to another agent, when so prompted. */
	private static final void handleMessagePrompt(final Trigger rawTrigger, final PlanToAgentInterface planInterface){
		final MessagePrompt<?> prompt = (MessagePrompt<?>) rawTrigger;
		final ReaderAgentContext<?> context = planInterface.getContext(ReaderAgentContext.class);
		final ArgumentationDialogProtocol<?> protocol = context.getProtocol();
		final ArgumentationMessage<?> response = new ArgumentationMessage<>(planInterface.getAgentID(), protocol.getSessionId(), prompt.getLocution(), prompt.getReceiver());
		ArgumentationDialogProtocol.send(planInterface, protocol, response);
	}
	 
	/** When a message has arrived successfully (it's legal) then prompt the next agent to do a move. */
	private static final void handleReceivedArgumentationMessage(final Trigger rawTrigger, final PlanToAgentInterface planInterface){
		final ArgumentationMessage<?> message = (ArgumentationMessage<?>) rawTrigger;
		if(message.isLegal())
			planInterface.getContext(ReaderAgentContext.class).promptNextAgent();
		else System.out.println("Message "+message.getContent()+" from "+message.getSender()+" in session "+message.getSessionId()+" is illegal.");
	} 
	
	// Communication capability setup

	/** Predicate to implement in order to determine whether to accept an invitation. */
	private static final boolean onInvited(final SessionInvitation invitation, final PlanToAgentInterface planInterface){
		// Initiate protocol dialog 
		final ReaderAgentContext<?> context = planInterface.getContext(ReaderAgentContext.class); 
		context.initiateDialog(planInterface, invitation.getSessionId());
		context.addPeer(invitation.getInviter(), true); 
		context.addPeer(planInterface.getAgentID(), false);
		return true;
	}

	/** Plan to implement when a peer has accepted an invitation. */
	private static final void onAccept(final CommunicationSessionId sessionId, final AgentID peer, final PlanToAgentInterface planInterface){ 
		System.out.println(peer+" has accepted to participate in "+sessionId);
		// Initiate the dialog 
		final ReaderAgentContext<?> context = planInterface.getContext(ReaderAgentContext.class);
		context.initiateDialog(planInterface, sessionId);
		context.addPeer(peer, false); // TODO: note that clash may occur if two agents invite each other, in which case both think they may start
		context.addPeer(planInterface.getAgentID(), true);  
		context.acceptedInvitation(peer); 
	}
	
	/** Plan to implement when the agent is organizing a session. Can be used to intialize some variables for the session for instance. */
	private static final void onOrganize(final SessionOrganizeTrigger organizeTrigger, final CommunicationSessionId sessionId, final PlanToAgentInterface planInterface){
		System.out.println(planInterface.getAgentID()+" is organizing session "+sessionId); 
	}

	/** Plan to implement when an invitation is rejected. */
	private static final void onReject(final CommunicationSessionId sessionId, final AgentID peer, final PlanToAgentInterface planInterface){ 
		System.out.println(peer+" has rejected to participate in "+sessionId);
	}

	/** Plan to implement when a peer has quit a session. */
	private static final void onQuit(final CommunicationSessionId sessionId, final AgentID peer, final PlanToAgentInterface planInterface){ 
		System.out.println(peer+" has quit "+sessionId);   
	} 
} 
