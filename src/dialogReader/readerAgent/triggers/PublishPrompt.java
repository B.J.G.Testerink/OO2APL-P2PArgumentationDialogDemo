package dialogReader.readerAgent.triggers;

import oo2apl.agent.Trigger;
/**
 * A trigger that is used to trigger agents into producing their dialog graphs as JSON objects and 
 * reporting them to the dialog reader.
 * 
 * @author Bas Testerink
 *
 */
public final class PublishPrompt implements Trigger {
	private static final PublishPrompt instance = new PublishPrompt();
	
	private PublishPrompt(){}
	
	public static final PublishPrompt getInstance(){ return instance; }
}
