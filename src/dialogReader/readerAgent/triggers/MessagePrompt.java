package dialogReader.readerAgent.triggers;
 
import oo2apl.agent.AgentID;
import oo2apl.agent.Trigger;
/**
 * 
 * A trigger that is used to prompt an agent to send a locution to the given receiver.
 * 
 * @author Bas Testerink
 *
 * @param <Locution>
 */
public final class MessagePrompt<Locution> implements Trigger {
	public final Locution locution;
	public final AgentID receiver;
	
	public MessagePrompt(final Locution locution, final AgentID receiver){
		this.locution = locution; 
		this.receiver = receiver;
	}
	
	public final Locution getLocution(){ return this.locution; }
	public final AgentID getReceiver(){ return this.receiver; }
}
